const wallet = {
    ownerName: 'Vitaliy Buterin',
    Bitcoin: {
        currencyName: 'Bitcoin',
        currencyLogo: '<img src="./img/Bitcoin_logo.png"></img>',
        value: 5,
        currencyRate: 10702.83
    },
    Ethereum: {
        currencyName: 'Ethereum',
        currencyLogo: '<img src="./img/Ethereum_logo.png"></img>',
        value: 1000000,
        currencyRate: 378.50
    },
    Stellar: {
        currencyName: 'Stellar',
        currencyLogo: '<img src="./img/Stellar_logo.png"></img>',
        value: 100,
        currencyRate: 0.081443
    },
    showInfo: function(coinName) {
        document.write('<p>Добрый день, ' + wallet.ownerName + '!<br> На Вашем балансе кошелька ' + this[coinName].currencyName + ' ' + 
        this[coinName].currencyLogo + ' осталось ' + this[coinName].value + ' монет.<br>Если Вы сегодня продадите их, то получите ' + 
        (this[coinName].value * this[coinName].currencyRate * 28).toFixed(2) + ' грн.</p><br>')
    }
};
wallet.showInfo('Stellar');
wallet.showInfo('Bitcoin');
wallet.showInfo('Ethereum');